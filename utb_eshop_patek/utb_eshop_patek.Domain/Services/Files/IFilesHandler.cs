﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Domain.Services.Files
{
    public interface IFilesHandler
    {
        string SaveImage(IFormFile file);
    }
}
