﻿using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Application.Client.ViewModels.Carts
{
    public class IndexViewModel : BaseViewModel
    {
        public IList<CartItemViewModel> CartItems { get; set; }
        public decimal Total { get; set; }
    }
}
