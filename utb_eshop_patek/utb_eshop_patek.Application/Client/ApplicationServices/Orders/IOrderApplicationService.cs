﻿using utb_eshop_patek.Application.Client.ViewModels.Orders;

namespace utb_eshop_patek.Application.Client.ApplicationServices.Orders
{
    public interface IOrderApplicationService
    {
        void CreateOrder(int userID, string userTrackingCode);
        IndexViewModel GetOrders(int userID);
    }
}
