﻿using utb_eshop_patek.Domain.Entities.Products;

namespace utb_eshop_patek.Domain.Entities.Carts
{
    public class CartItem : Entity
    {
        public int CartID { get; set; }
        public int ProductID { get; set; }
        public int Amount { get; set; }

        public Cart Cart { get; set; }
        public Product Product { get; set; }
    }
}